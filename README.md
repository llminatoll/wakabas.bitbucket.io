[![Netlify Status](https://api.netlify.com/api/v1/badges/6601bcdb-78e1-457e-af58-bc1e5b11be12/deploy-status)](https://app.netlify.com/sites/infallible-jennings-cd8cb5/deploys)

# 真央ゼミWebサイト
- 本リポジトリは勉強会での練習として使用するリポジトリです。
- プルリクエストを送ると、真央ゼミWebサイトの情報を追加更新できます。
   - 基本的には好きな内容を追加してOKですが、見た人が気分を害するもの・第三者の著作権を侵害するものは反映できませんのでご了承ください。

Netlifyの機能を使って公開されているページは[こちら](https://infallible-jennings-cd8cb5.netlify.com/)から閲覧できます。 https://llminatoll.github.io/my-seminar/


# ディレクトリ構成
```
seminar/
├─ index.html        # トップページ
├─ member.html       # メンバー紹介ページ
├─ work.html         # ゼミ活動紹介ページ
├─ README.md
├─ images
└─ css/
    └─ common.css
```
